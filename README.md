# reconcilement

## Requirements

* Gradle 4+
* JDK 9
* Spock
* Feign
* PubSub
* StackDriver

## Packages

### application
must contain the spring application

### config
must contain the configuration
 
### gateway
must contain the external interface

### presentation
must contain the external presentation interface
 
### usecase
must contain the business rules