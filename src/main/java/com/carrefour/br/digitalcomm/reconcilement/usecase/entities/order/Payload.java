package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Payload.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payload {
    private String orderGroup;
    private String principalCode;
    private String orderCode;
    private String orderOrigin;
    private String affiliatedId;
    private String marketplaceOrderId;
    private String sellerId;
    private String sellerOrderId;
    private Customer customer;
    private List<Payment> payments;
    private List<Consignment> consignments;
}
