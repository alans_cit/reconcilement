package com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions;

/**
 * The type User not found exception.
 */
public class UserNotFoundException extends RuntimeException {
}
