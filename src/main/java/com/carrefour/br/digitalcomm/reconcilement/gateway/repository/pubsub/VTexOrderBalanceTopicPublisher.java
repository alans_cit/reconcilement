package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.pubsub;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.balance.VTexOrderBalanceDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;

/**
 * The type V tex order balance topic publisher.
 */
@Slf4j
public class VTexOrderBalanceTopicPublisher extends PubSubPublisher implements PubSubRepository<VTexOrderBalanceDocument> {
    @Value("${message.balance-topic-name}")
    private String publisherTopicName;

    /**
     * Instantiates a new V tex order balance topic publisher.
     *
     * @param pubSubTemplate the pub sub template
     */
    public VTexOrderBalanceTopicPublisher(PubSubTemplate pubSubTemplate) {
        super(pubSubTemplate);
    }

    @Override
    public void publish(VTexOrderBalanceDocument message) {
        super.publish(message.toString());
    }

    @Override
    protected String topic() {
        return publisherTopicName;
    }
}
