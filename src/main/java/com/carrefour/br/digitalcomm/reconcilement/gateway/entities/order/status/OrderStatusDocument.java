package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.status;

/**
 * The type Order status document.
 */
public class OrderStatusDocument {
    /**
     * The constant COLLECTION_NAME.
     */
    public static final String COLLECTION_NAME = "reconcilement_order_status";
}
