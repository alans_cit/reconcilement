package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.user;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface User feign response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserFeignResponseMapper {

    /**
     * The constant INSTANCE.
     */
    UserFeignResponseMapper INSTANCE = Mappers.getMapper(UserFeignResponseMapper.class);

    /**
     * Document to entity user.
     *
     * @param document the document
     * @return the user
     */
    User documentToEntity(UserFeignResponse document);

    /**
     * Document to entity list.
     *
     * @param document the document
     * @return the list
     */
    List<User> documentToEntity(List<UserFeignResponse> document);
}
