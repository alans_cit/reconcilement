package com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * The type Invalid v tex order balance exception.
 */
@Getter
@RequiredArgsConstructor
public class InvalidVTexOrderBalanceException extends RuntimeException {
    private final String message;
}
