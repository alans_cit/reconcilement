package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.pubsub;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;

/**
 * The type Pub sub publisher.
 */
@Slf4j
@RequiredArgsConstructor
public abstract class PubSubPublisher {
    private final PubSubTemplate pubSubTemplate;

    /**
     * Topic string.
     *
     * @return the string
     */
    protected abstract String topic();

    /**
     * Publish.
     *
     * @param message the message
     */
    public void publish(String message) {
        log.info("publishing to topic: {} the message: {}", topic(), message);
        pubSubTemplate.publish(topic(), message);
    }
}
