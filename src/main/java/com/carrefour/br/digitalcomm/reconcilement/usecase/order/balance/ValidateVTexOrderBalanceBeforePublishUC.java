package com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.InvalidVTexOrderBalanceException;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * The type Validate v tex order balance before publish uc.
 */
public class ValidateVTexOrderBalanceBeforePublishUC {
    /**
     * The constant ORDER_BALANCE_CAN_NOT_BE_NULL.
     */
    public static final String ORDER_BALANCE_CAN_NOT_BE_NULL = "orderBalance can not be null";
    private static final String ORDER_BALANCE_CODE_CAN_NOT_BE_EMPTY = "orderBalance code can not be empty";
    private static final String ORDER_BALANCE_STATUS_CAN_NOT_BE_EMPTY = "orderBalance status can not be empty";

    /**
     * Execute boolean.
     *
     * @param orderBalance the order balance
     * @return the boolean
     */
    public boolean execute(VTexOrderBalance orderBalance) {
        if (Objects.isNull(orderBalance))
            throw new InvalidVTexOrderBalanceException(ORDER_BALANCE_CAN_NOT_BE_NULL);

        if (StringUtils.isEmpty(orderBalance.getCode()))
            throw new InvalidVTexOrderBalanceException(ORDER_BALANCE_CODE_CAN_NOT_BE_EMPTY);

        if (StringUtils.isEmpty(orderBalance.getStatus()))
            throw new InvalidVTexOrderBalanceException(ORDER_BALANCE_STATUS_CAN_NOT_BE_EMPTY);

        return Boolean.TRUE;
    }
}
