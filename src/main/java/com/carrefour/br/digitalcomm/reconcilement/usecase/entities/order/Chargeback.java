package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.*;

/**
 * The type Chargeback.
 */
@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Chargeback {
    private String paymentId;
    private String requestId;
}
