package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Chargeback document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChargebackDocument {
    private String paymentId;
    private String requestId;
}
