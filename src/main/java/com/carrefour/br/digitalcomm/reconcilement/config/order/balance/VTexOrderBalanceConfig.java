package com.carrefour.br.digitalcomm.reconcilement.config.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.gateway.order.balance.VTexOrderBalanceGateway;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.pubsub.VTexOrderBalanceTopicPublisher;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance.GenerateBalanceUC;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance.ValidateVTexOrderBalanceBeforePublishUC;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type V tex order balance config.
 */
@Configuration
public class VTexOrderBalanceConfig {
    /**
     * Gets generate balance uc.
     *
     * @param vTexOrderBalanceGateway                 the v tex order balance gateway
     * @param validateVTexOrderBalanceBeforePublishUC the validate v tex order balance before publish uc
     * @return the generate balance uc
     */
    @Bean
    public GenerateBalanceUC getGenerateBalanceUC(final VTexOrderBalanceGateway vTexOrderBalanceGateway,
                                                  final ValidateVTexOrderBalanceBeforePublishUC validateVTexOrderBalanceBeforePublishUC) {
        return new GenerateBalanceUC(vTexOrderBalanceGateway, validateVTexOrderBalanceBeforePublishUC);
    }

    /**
     * Gets v tex order balance gateway.
     *
     * @param repository the repository
     * @return the v tex order balance gateway
     */
    @Bean
    public VTexOrderBalanceGateway getVTexOrderBalanceGateway(final VTexOrderBalanceTopicPublisher repository) {
        return new VTexOrderBalanceGateway(repository);
    }

    /**
     * Gets v tex order balance topic publisher.
     *
     * @param pubSubTemplate the pub sub template
     * @return the v tex order balance topic publisher
     */
    @Bean
    public VTexOrderBalanceTopicPublisher getVTexOrderBalanceTopicPublisher(final PubSubTemplate pubSubTemplate) {
        return new VTexOrderBalanceTopicPublisher(pubSubTemplate);
    }

    /**
     * Gets validate v tex order balance before publish uc.
     *
     * @return the validate v tex order balance before publish uc
     */
    @Bean
    public ValidateVTexOrderBalanceBeforePublishUC getValidateVTexOrderBalanceBeforePublishUC() {
        return new ValidateVTexOrderBalanceBeforePublishUC();
    }
}
