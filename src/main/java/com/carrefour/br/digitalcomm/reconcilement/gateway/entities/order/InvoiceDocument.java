package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Invoice document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDocument {
    private String type;
    private String number;
    private String serial;
    private String key;
}
