package com.carrefour.br.digitalcomm.reconcilement.gateway.order;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocument;
import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocumentMapper;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore.VTexOrderRepository;
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund.SaveVTexOrder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * The type V tex order gateway.
 */
@Slf4j
public class VTexOrderGateway implements SaveVTexOrder {

    private final VTexOrderRepository repository;

    /**
     * Instantiates a new V tex order gateway.
     *
     * @param repository the repository
     */
    public VTexOrderGateway(VTexOrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(final VTexOrder vTexOrder) {
        log.debug("saving cancel transaction with code: {}", vTexOrder.getCode());

        repository.save(VTexOrderDocumentMapper.INSTANCE.entityToDocument(vTexOrder));
    }

    /**
     * Gets by status.
     *
     * @param status the status
     * @return the by status
     */
    public List<VTexOrder> getByStatus(String status) {
        log.debug("getting vtex order by status: {}", status);

        return VTexOrderDocumentMapper.INSTANCE.documentToEntity(repository.getBy(VTexOrderDocument.STATUS, status));
    }

    /**
     * Gets by payment id.
     *
     * @param paymentId the payment id
     * @return the by payment id
     */
    public List<VTexOrder> getByPaymentId(String paymentId) {
        log.debug("Get vtex order by paymentId: {}", paymentId);

        return VTexOrderDocumentMapper.INSTANCE.documentToEntity(repository.getByArrayContains(VTexOrderDocument.PAYMENT_IDS, paymentId));
    }
}
