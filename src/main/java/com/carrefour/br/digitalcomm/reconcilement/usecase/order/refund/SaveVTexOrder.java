package com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder;

/**
 * The interface Save v tex order.
 */
public interface SaveVTexOrder {
    /**
     * Save.
     *
     * @param vTexOrder the v tex order
     */
    void save(VTexOrder vTexOrder);
}
