package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * The interface V tex order balance document mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VTexOrderBalanceDocumentMapper {

    /**
     * The constant INSTANCE.
     */
    VTexOrderBalanceDocumentMapper INSTANCE = Mappers.getMapper(VTexOrderBalanceDocumentMapper.class);

    /**
     * Entity to document v tex order balance document.
     *
     * @param entity the entity
     * @return the v tex order balance document
     */
    VTexOrderBalanceDocument entityToDocument(VTexOrderBalance entity);

    /**
     * Document to entity v tex order balance.
     *
     * @param document the document
     * @return the v tex order balance
     */
    VTexOrderBalance documentToEntity(VTexOrderBalanceDocument document);
}
