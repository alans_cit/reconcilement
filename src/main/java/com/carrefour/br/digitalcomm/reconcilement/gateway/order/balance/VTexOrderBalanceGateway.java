package com.carrefour.br.digitalcomm.reconcilement.gateway.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.balance.VTexOrderBalanceDocumentMapper;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.pubsub.VTexOrderBalanceTopicPublisher;
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance.PublishVTexOrderBalanceInTopic;
import lombok.extern.slf4j.Slf4j;

/**
 * The type V tex order balance gateway.
 */
@Slf4j
public class VTexOrderBalanceGateway implements PublishVTexOrderBalanceInTopic {
    private final VTexOrderBalanceTopicPublisher repository;

    /**
     * Instantiates a new V tex order balance gateway.
     *
     * @param repository the repository
     */
    public VTexOrderBalanceGateway(VTexOrderBalanceTopicPublisher repository) {
        this.repository = repository;
    }

    @Override
    public void publish(VTexOrderBalance orderBalance) {
        log.debug("publishing order balance: {}", orderBalance);
        repository.publish(VTexOrderBalanceDocumentMapper.INSTANCE.entityToDocument(orderBalance));
    }
}
