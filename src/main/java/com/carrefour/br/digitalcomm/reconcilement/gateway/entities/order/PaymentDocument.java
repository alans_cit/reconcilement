package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Payment document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDocument {
    private String paymentId;
    private String paymentSystem;
    private String cardNumber;
    private Long value ;
}
