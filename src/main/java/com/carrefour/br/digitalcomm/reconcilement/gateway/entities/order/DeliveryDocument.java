package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Delivery document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDocument {
    private String modality;
    private Double totalFreight;
    private String warehouse;
    private String estimate;
    private String startWindow;
    private String endWindow;
    private String receiverName;
    private String receiverDocument;
    private String store;
    private String pickupModality;
    private ShippingAddressDocument shippingAddress;
}
