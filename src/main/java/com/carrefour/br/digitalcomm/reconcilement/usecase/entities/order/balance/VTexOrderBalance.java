package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type V tex order balance.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VTexOrderBalance {
    private String code;
    private String status;
}
