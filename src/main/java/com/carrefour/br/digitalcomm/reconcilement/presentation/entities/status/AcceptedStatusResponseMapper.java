package com.carrefour.br.digitalcomm.reconcilement.presentation.entities.status;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Objects;

/**
 * The interface Accepted status response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AcceptedStatusResponseMapper {

    /**
     * The constant INSTANCE.
     */
    AcceptedStatusResponseMapper INSTANCE = Mappers.getMapper(AcceptedStatusResponseMapper.class);

    /**
     * Entity to response accepted status response.
     *
     * @param entities the entities
     * @return the accepted status response
     */
    default AcceptedStatusResponse entityToResponse(List<String> entities) {
        if (Objects.isNull(entities))
            return null;

        return AcceptedStatusResponse.builder()
                .status(entities)
                .build();
    }
}
