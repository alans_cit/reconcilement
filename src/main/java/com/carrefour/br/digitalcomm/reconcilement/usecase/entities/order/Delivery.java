package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Delivery.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Delivery {
    private String modality;
    private Double totalFreight;
    private String warehouse;
    private String estimate;
    private String startWindow;
    private String endWindow;
    private String receiverName;
    private String receiverDocument;
    private String store;
    private String pickupModality;
    private ShippingAddress shippingAddress;
}
