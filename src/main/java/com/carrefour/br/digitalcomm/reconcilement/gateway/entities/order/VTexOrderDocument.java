package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import com.google.cloud.firestore.annotation.DocumentId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type V tex order document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VTexOrderDocument {
    /**
     * The constant COLLECTION_NAME.
     */
    public static final String COLLECTION_NAME = "payment_vtex_order";
    /**
     * The constant CODE.
     */
    public static final String CODE = "code";
    /**
     * The constant STATUS.
     */
    public static final String STATUS = "status";
    /**
     * The constant PAYMENT_IDS.
     */
    public static final String PAYMENT_IDS = "paymentIds";

    @DocumentId
    private String id;

    private List<String> paymentIds;
    private List<ChargebackDocument> chargebacks;
    private String code;
    private String status;
    private String description;
    private String statusTime;
    private PayloadDocument payload;
}
