package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.pubsub;

/**
 * The interface Pub sub repository.
 *
 * @param <T> the type parameter
 */
public interface PubSubRepository<T> {
    /**
     * Publish.
     *
     * @param message the message
     */
    void publish(T message);
}
