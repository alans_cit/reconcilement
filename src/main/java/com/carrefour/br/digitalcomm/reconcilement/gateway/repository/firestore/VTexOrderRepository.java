package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocument;
import com.google.cloud.firestore.Firestore;
import lombok.extern.slf4j.Slf4j;

/**
 * The type V tex order repository.
 */
@Slf4j
public class VTexOrderRepository extends FirestoreGenericRepository<VTexOrderDocument> {

    /**
     * Instantiates a new V tex order repository.
     *
     * @param collection the collection
     * @param firestore  the firestore
     */
    public VTexOrderRepository(String collection, Firestore firestore) {
        super(collection, firestore);
    }

}
