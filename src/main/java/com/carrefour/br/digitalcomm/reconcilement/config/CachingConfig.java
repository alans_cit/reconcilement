package com.carrefour.br.digitalcomm.reconcilement.config;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.status.OrderStatusDocument;
import com.google.common.cache.CacheBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * The type Caching config.
 */
@Configuration
@EnableCaching
public class CachingConfig {
    /**
     * Cache manager cache manager.
     *
     * @return the cache manager
     */
    @Bean
    public CacheManager cacheManager() {
        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager() {
            @Override
            protected Cache createConcurrentMapCache(final String name) {
                return new ConcurrentMapCache(name, CacheBuilder.newBuilder()
                        .expireAfterWrite(60, TimeUnit.SECONDS)
                        .maximumSize(100).build().asMap(), true);
            }
        };
        cacheManager.setCacheNames(Arrays.asList(OrderStatusDocument.COLLECTION_NAME));

        return cacheManager;
    }
}
