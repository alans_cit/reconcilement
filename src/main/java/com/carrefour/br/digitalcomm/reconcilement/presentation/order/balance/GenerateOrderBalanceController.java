package com.carrefour.br.digitalcomm.reconcilement.presentation.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.balance.VTexOrderBalanceRequest;
import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.balance.VTexOrderBalanceRequestMapper;
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.InvalidVTexOrderBalanceException;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance.GenerateBalanceUC;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * The type Generate order balance controller.
 */
@Slf4j
@RestController
@RequestMapping(value = "/v1/reconcilement")
@RequiredArgsConstructor
public class GenerateOrderBalanceController {
    private final GenerateBalanceUC generateBalanceUC;

    /**
     * Create.
     *
     * @param request the request
     */
    @PostMapping(value = "/order/balance", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody VTexOrderBalanceRequest request) {
        generateBalanceUC.execute(VTexOrderBalanceRequestMapper.INSTANCE.requestToEntity(request));
    }

    /**
     * Handle invalid v tex order balance exception response entity.
     *
     * @param e the e
     * @return the response entity
     */
    @ExceptionHandler({InvalidVTexOrderBalanceException.class})
    public ResponseEntity<Void> handleInvalidVTexOrderBalanceException(InvalidVTexOrderBalanceException e) {
        log.error("order balance invalid");

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
