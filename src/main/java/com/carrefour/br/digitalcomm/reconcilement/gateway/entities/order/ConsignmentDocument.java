package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Consignment document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConsignmentDocument {
    private String consignmentCode;
    private String type;
    private Double totalValue;
    private Double totalItemValue;
    private Double totalDiscounts;
    private List<ItemDocument> items;
    private List<InvoiceDocument> invoices;
    private InvoiceAddressDocument invoiceAddress;
    private DeliveryDocument delivery;
}
