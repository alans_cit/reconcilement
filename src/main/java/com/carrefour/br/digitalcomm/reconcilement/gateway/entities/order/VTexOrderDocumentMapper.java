package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface V tex order document mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VTexOrderDocumentMapper {

    /**
     * The constant INSTANCE.
     */
    VTexOrderDocumentMapper INSTANCE = Mappers.getMapper(VTexOrderDocumentMapper.class);

    /**
     * Entity to document v tex order document.
     *
     * @param entity the entity
     * @return the v tex order document
     */
    VTexOrderDocument entityToDocument(VTexOrder entity);

    /**
     * Document to entity v tex order.
     *
     * @param document the document
     * @return the v tex order
     */
    VTexOrder documentToEntity(VTexOrderDocument document);

    /**
     * Document to entity list.
     *
     * @param document the document
     * @return the list
     */
    List<VTexOrder> documentToEntity(List<VTexOrderDocument> document);
}
