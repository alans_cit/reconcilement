package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.pubsub;

import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gcp.pubsub.support.converter.PubSubMessageConverter;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * The type Gson pub sub message mapper.
 */
@Slf4j
public class GsonPubSubMessageMapper implements PubSubMessageConverter {

    private final Charset charset;
    private final Gson gson;

    /**
     * Instantiates a new Gson pub sub message mapper.
     */
    public GsonPubSubMessageMapper() {
        charset = Charset.defaultCharset();
        gson = new Gson();
    }

    @Override
    public PubsubMessage toPubSubMessage(Object payload, Map<String, String> headers) {
        log.info("Convert payload to PubSub Message");
        PubsubMessage.Builder pubsubMessageBuilder = PubsubMessage.newBuilder()
                .setData(ByteString.copyFrom(gson.toJson(payload).getBytes(this.charset)));

        if (headers != null) {
            pubsubMessageBuilder.putAllAttributes(headers);
        }

        return pubsubMessageBuilder.build();
    }

    @Override
    public <T> T fromPubSubMessage(PubsubMessage message, Class<T> payloadType) {
        log.info("Start to convert PubSub Message to payload type: {}", payloadType);
        byte[] dataArray = message.getData().toByteArray();
        String payload = new String(dataArray, this.charset);
        return gson.fromJson(payload, payloadType);
    }
}
