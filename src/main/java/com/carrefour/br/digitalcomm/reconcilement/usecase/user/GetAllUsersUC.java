package com.carrefour.br.digitalcomm.reconcilement.usecase.user;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User;
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * The type Get all users uc.
 */
@Slf4j
@RequiredArgsConstructor
public class GetAllUsersUC {
    private final GetAllUsers getAllUsers;

    /**
     * Execute list.
     *
     * @return the list
     */
    public List<User> execute() {
        log.info("getting all users");
        List<User> users = getAllUsers.getAll();

        if (CollectionUtils.isEmpty(users)) {
            log.warn("list of users is null");
            throw new UserNotFoundException();
        }

        return users;
    }
}
