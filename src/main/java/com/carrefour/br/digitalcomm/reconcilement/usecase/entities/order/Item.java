package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Item.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    private String type;
    private String vtexItemId;
    private String sellerSku;
    private String bundleItem;
    private String ean;
    private Double discount;
    private Integer quantity;
    private Double height;
    private Double length;
    private Double weight;
    private Double width;
    private Double price;
    private Double basePrice;
    private Double freightPrice;
    private Boolean gift;
}
