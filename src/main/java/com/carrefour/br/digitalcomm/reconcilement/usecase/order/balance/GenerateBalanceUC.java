package com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Generate balance uc.
 */
@Slf4j
@RequiredArgsConstructor
public class GenerateBalanceUC {
    private final PublishVTexOrderBalanceInTopic publishVTexOrderBalanceInTopic;
    private final ValidateVTexOrderBalanceBeforePublishUC validateVTexOrderBalanceBeforePublishUC;

    /**
     * Execute.
     *
     * @param orderBalance the order balance
     */
    public void execute(VTexOrderBalance orderBalance) {
        validateVTexOrderBalanceBeforePublishUC.execute(orderBalance);
        publishVTexOrderBalanceInTopic.publish(orderBalance);
    }
}
