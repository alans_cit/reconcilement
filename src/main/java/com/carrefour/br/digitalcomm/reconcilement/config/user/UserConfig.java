package com.carrefour.br.digitalcomm.reconcilement.config.user;

import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.feign.PlaceHolderUserRepository;
import com.carrefour.br.digitalcomm.reconcilement.gateway.user.UserGateway;
import com.carrefour.br.digitalcomm.reconcilement.usecase.user.GetAllUsers;
import com.carrefour.br.digitalcomm.reconcilement.usecase.user.GetAllUsersUC;
import feign.Feign;
import feign.Logger;
import feign.slf4j.Slf4jLogger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type User config.
 */
@Configuration
@RequiredArgsConstructor
public class UserConfig {
    @Value("${feign.placeholder.users.url}")
    private String userRepositoryUrl;

    /**
     * Gets get all users uc.
     *
     * @param getAllUsers the get all users
     * @return the get all users uc
     */
    @Bean
    public GetAllUsersUC getGetAllUsersUC(final GetAllUsers getAllUsers) {
        return new GetAllUsersUC(getAllUsers);
    }

    /**
     * Gets user gateway.
     *
     * @param placeHolderUserRepository the place holder user repository
     * @return the user gateway
     */
    @Bean
    public UserGateway getUserGateway(final PlaceHolderUserRepository placeHolderUserRepository) {
        return new UserGateway(placeHolderUserRepository);
    }

    /**
     * Gets place holder user repository.
     *
     * @return the place holder user repository
     */
    @Bean
    public PlaceHolderUserRepository getPlaceHolderUserRepository() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(PlaceHolderUserRepository.class))
                .logLevel(Logger.Level.FULL)
                .target(PlaceHolderUserRepository.class, userRepositoryUrl);
    }
}
