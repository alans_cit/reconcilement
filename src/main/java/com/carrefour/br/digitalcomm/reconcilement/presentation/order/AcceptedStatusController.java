package com.carrefour.br.digitalcomm.reconcilement.presentation.order;

import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.status.AcceptedStatusResponse;
import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.status.AcceptedStatusResponseMapper;
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.StatusOrderAcceptedNotFoundException;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAcceptedUC;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * The type Accepted status controller.
 */
@Slf4j
@RestController
@RequestMapping(value = "/v1/reconcilement")
@RequiredArgsConstructor
public class AcceptedStatusController {

    private final GetListOfStatusOrderAcceptedUC getListOfStatusOrderAcceptedUC;

    /**
     * Accepted statuses accepted status response.
     *
     * @return the accepted status response
     */
    @GetMapping("/accepted-status")
    @ResponseStatus(HttpStatus.OK)
    public AcceptedStatusResponse AcceptedStatuses() {
        log.info("getting list of status order accepted");
        return AcceptedStatusResponseMapper.INSTANCE.entityToResponse(getListOfStatusOrderAcceptedUC.execute());
    }

    /**
     * Handle status order accepted not found exception response entity.
     *
     * @param e the e
     * @return the response entity
     */
    @ExceptionHandler({StatusOrderAcceptedNotFoundException.class})
    public ResponseEntity<Void> handleStatusOrderAcceptedNotFoundException(StatusOrderAcceptedNotFoundException e) {
        log.error("accepted status not found");

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
