package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Consignment.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Consignment {
    private String consignmentCode;
    private String type;
    private Double totalValue;
    private Double totalItemValue;
    private Double totalDiscounts;
    private List<Item> items;
    private List<Invoice> invoices;
    private InvoiceAddress invoiceAddress;
    private Delivery delivery;
}
