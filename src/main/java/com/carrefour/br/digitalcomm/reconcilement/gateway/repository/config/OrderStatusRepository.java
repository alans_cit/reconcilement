package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.config;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.status.OrderStatusDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;

import java.util.Arrays;
import java.util.List;

/**
 * The type Order status repository.
 */
@Slf4j
public class OrderStatusRepository  {
    @Value("${message.status.accepted}")
    private String[] statusAccepted;

    /**
     * Gets status accepted.
     *
     * @return the status accepted
     */
    @Cacheable(OrderStatusDocument.COLLECTION_NAME)
    public List<String> getStatusAccepted() {
        log.debug("getting status accepted");
        return Arrays.asList(statusAccepted);
    }
}
