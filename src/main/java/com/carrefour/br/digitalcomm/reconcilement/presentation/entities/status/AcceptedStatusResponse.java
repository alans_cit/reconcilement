package com.carrefour.br.digitalcomm.reconcilement.presentation.entities.status;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Accepted status response.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AcceptedStatusResponse {
    /**
     * The Status.
     */
    List<String> status;
}
