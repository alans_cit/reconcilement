package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Customer document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDocument {
    private String firstName;
    private String lastName;
    private String documentType;
    private String document;
    private String phone;
    private String email;
}
