package com.carrefour.br.digitalcomm.reconcilement.config.order;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocument;
import com.carrefour.br.digitalcomm.reconcilement.gateway.order.VTexOrderGateway;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore.VTexOrderRepository;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAccepted;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAcceptedUC;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund.ProcessVTexOrderUC;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund.SaveVTexOrder;
import com.google.cloud.firestore.Firestore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type V tex order config.
 */
@Configuration
public class VTexOrderConfig {

    /**
     * Gets v tex order gateway.
     *
     * @param database the database
     * @return the v tex order gateway
     */
    @Bean
    public VTexOrderGateway getVTexOrderGateway(final VTexOrderRepository database) {
        return new VTexOrderGateway(database);
    }

    /**
     * Gets v tex order database.
     *
     * @param firestore the firestore
     * @return the v tex order database
     */
    @Bean
    public VTexOrderRepository getVTexOrderDatabase(final Firestore firestore) {
        return new VTexOrderRepository(VTexOrderDocument.COLLECTION_NAME, firestore);
    }

    /**
     * Gets get list of status order accepted uc.
     *
     * @param getListOfStatusOrderAccepted the get list of status order accepted
     * @return the get list of status order accepted uc
     */
    @Bean
    public GetListOfStatusOrderAcceptedUC getGetListOfStatusOrderAcceptedUC(final GetListOfStatusOrderAccepted getListOfStatusOrderAccepted) {
        return new GetListOfStatusOrderAcceptedUC(getListOfStatusOrderAccepted);
    }

    /**
     * Gets process v tex order uc.
     *
     * @param saveVTexOrder                  the save v tex order
     * @param getListOfStatusOrderAcceptedUC the get list of status order accepted uc
     * @return the process v tex order uc
     */
    @Bean
    public ProcessVTexOrderUC getProcessVTexOrderUC(final SaveVTexOrder saveVTexOrder,final GetListOfStatusOrderAcceptedUC getListOfStatusOrderAcceptedUC) {
        return new ProcessVTexOrderUC(saveVTexOrder, getListOfStatusOrderAcceptedUC);
    }

}
