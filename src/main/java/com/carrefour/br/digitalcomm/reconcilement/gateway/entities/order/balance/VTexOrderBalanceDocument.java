package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type V tex order balance document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VTexOrderBalanceDocument {
    private String code;
    private String status;
}
