package com.carrefour.br.digitalcomm.reconcilement.config.order;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.pubsub.GsonPubSubMessageMapper;
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund.ProcessVTexOrderUC;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;

/**
 * The type Reconcilement topic in config.
 */
@Configuration
@RequiredArgsConstructor
public class ReconcilementTopicInConfig {
    private final String CHANNEL = "pubSubInputChannel";

    @Value("${message.subscription-name}")
    private String subscriptionName;

    private final ProcessVTexOrderUC processVTexOrderUC;

    /**
     * Gets process v tex order uc.
     *
     * @return the process v tex order uc
     */
    public ProcessVTexOrderUC getProcessVTexOrderUC() {
        return processVTexOrderUC;
    }

    /**
     * Pub sub input channel direct channel.
     *
     * @return the direct channel
     */
    @Bean
    public DirectChannel pubSubInputChannel() {
        return new DirectChannel();
    }

    /**
     * Message channel adapter pub sub inbound channel adapter.
     *
     * @param inputChannel   the input channel
     * @param pubSubTemplate the pub sub template
     * @return the pub sub inbound channel adapter
     */
    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(@Qualifier(CHANNEL) MessageChannel inputChannel,
                                                             PubSubTemplate pubSubTemplate) {
        pubSubTemplate.setMessageConverter(new GsonPubSubMessageMapper());
        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, subscriptionName);
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL);
        adapter.setPayloadType(VTexOrder.class);
        return adapter;
    }

    /**
     * Message receiver.
     *
     * @param vTexOrder the v tex order
     * @param message   the message
     */
    @ServiceActivator(inputChannel = CHANNEL)
    public void messageReceiver(VTexOrder vTexOrder,
                                @Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage message) {
        processVTexOrderUC.execute(vTexOrder);
        message.ack();
    }
}
