package com.carrefour.br.digitalcomm.reconcilement.presentation.entities.balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type V tex order balance request.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VTexOrderBalanceRequest {
    private String code;
    private String status;
}
