package com.carrefour.br.digitalcomm.reconcilement.gateway.user;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.user.UserFeignResponseMapper;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.feign.PlaceHolderUserRepository;
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User;
import com.carrefour.br.digitalcomm.reconcilement.usecase.user.GetAllUsers;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * The type User gateway.
 */
@Slf4j
public class UserGateway implements GetAllUsers {
    private final PlaceHolderUserRepository repository;

    /**
     * Instantiates a new User gateway.
     *
     * @param repository the repository
     */
    public UserGateway(PlaceHolderUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAll() {
        return UserFeignResponseMapper.INSTANCE.documentToEntity(repository.getAllUsers());
    }
}
