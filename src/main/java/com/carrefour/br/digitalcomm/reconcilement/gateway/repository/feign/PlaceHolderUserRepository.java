package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.feign;

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.user.UserFeignResponse;
import feign.RequestLine;

import java.util.List;

/**
 * The interface Place holder user repository.
 */
public interface PlaceHolderUserRepository {
    /**
     * Gets all users.
     *
     * @return the all users
     */
    @RequestLine("GET /users")
    List<UserFeignResponse> getAllUsers();
}
