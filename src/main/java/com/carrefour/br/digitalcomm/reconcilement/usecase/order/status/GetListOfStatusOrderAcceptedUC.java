package com.carrefour.br.digitalcomm.reconcilement.usecase.order.status;

import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.StatusOrderAcceptedNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * The type Get list of status order accepted uc.
 */
@Slf4j
@RequiredArgsConstructor
public class GetListOfStatusOrderAcceptedUC {

    private final GetListOfStatusOrderAccepted getListOfStatusOrderAccepted;

    /**
     * Execute list.
     *
     * @return the list
     */
    public List<String> execute() {
        log.info("getting list of status order accepted");
        List<String> statuses = getListOfStatusOrderAccepted.getStatusAccepted();

        if (CollectionUtils.isEmpty(statuses)) {
            log.warn("list of status order accepted is null");
            throw new StatusOrderAcceptedNotFoundException();
        }

        return statuses;
    }
}
