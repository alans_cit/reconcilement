package com.carrefour.br.digitalcomm.reconcilement.gateway.order;

import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.config.OrderStatusRepository;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAccepted;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * The type Order status gateway.
 */
@Slf4j
public class OrderStatusGateway implements GetListOfStatusOrderAccepted {
    private final OrderStatusRepository repository;

    /**
     * Instantiates a new Order status gateway.
     *
     * @param repository the repository
     */
    public OrderStatusGateway(OrderStatusRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<String> getStatusAccepted() {
        log.debug("getting status accepted");
        return repository.getStatusAccepted();
    }
}
