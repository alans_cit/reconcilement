package com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;

/**
 * The interface Publish v tex order balance in topic.
 */
public interface PublishVTexOrderBalanceInTopic {
    /**
     * Publish.
     *
     * @param orderBalance the order balance
     */
    void publish(VTexOrderBalance orderBalance);
}
