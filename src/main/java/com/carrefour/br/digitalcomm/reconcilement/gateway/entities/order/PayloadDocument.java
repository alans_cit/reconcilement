package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Payload document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PayloadDocument {
    private String orderGroup;
    private String principalCode;
    private String orderCode;
    private String orderOrigin;
    private String affiliatedId;
    private String marketplaceOrderId;
    private String sellerId;
    private String sellerOrderId;
    private CustomerDocument customer;
    private List<PaymentDocument> payments;
    private List<ConsignmentDocument> consignments;
}
