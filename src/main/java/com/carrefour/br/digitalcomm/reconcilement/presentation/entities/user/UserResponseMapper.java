package com.carrefour.br.digitalcomm.reconcilement.presentation.entities.user;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface User response mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserResponseMapper {
    /**
     * The constant INSTANCE.
     */
    UserResponseMapper INSTANCE = Mappers.getMapper(UserResponseMapper.class);

    /**
     * Entity to response user response.
     *
     * @param entity the entity
     * @return the user response
     */
    UserResponse entityToResponse(User entity);

    /**
     * Entity to response list.
     *
     * @param entity the entity
     * @return the list
     */
    List<UserResponse> entityToResponse(List<User> entity);
}
