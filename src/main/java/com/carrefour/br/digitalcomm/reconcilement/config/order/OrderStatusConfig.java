package com.carrefour.br.digitalcomm.reconcilement.config.order;

import com.carrefour.br.digitalcomm.reconcilement.gateway.order.OrderStatusGateway;
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.config.OrderStatusRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Order status config.
 */
@Configuration
public class OrderStatusConfig {
    /**
     * Gets order status gateway.
     *
     * @param orderStatusRepository the order status repository
     * @return the order status gateway
     */
    @Bean
    public OrderStatusGateway getOrderStatusGateway(final OrderStatusRepository orderStatusRepository) {
        return new OrderStatusGateway(orderStatusRepository);
    }

    /**
     * Gets order status repository.
     *
     * @return the order status repository
     */
    @Bean
    public OrderStatusRepository getOrderStatusRepository() {
        return new OrderStatusRepository();
    }
}
