package com.carrefour.br.digitalcomm.reconcilement.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * The type Application.
 */
@EnableFeignClients
@EnableAsync
@EnableConfigurationProperties()
@SpringBootApplication(scanBasePackages = "com.carrefour.*")
public class Application {
    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
