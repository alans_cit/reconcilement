package com.carrefour.br.digitalcomm.reconcilement.usecase.user;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User;

import java.util.List;

/**
 * The interface Get all users.
 */
public interface GetAllUsers {
    /**
     * Gets all.
     *
     * @return the all
     */
    List<User> getAll();
}
