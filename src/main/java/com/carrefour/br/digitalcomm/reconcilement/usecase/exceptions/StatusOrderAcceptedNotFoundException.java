package com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions;

/**
 * The type Status order accepted not found exception.
 */
public class StatusOrderAcceptedNotFoundException extends RuntimeException {
}
