package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Invoice.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private String type;
    private String number;
    private String serial;
    private String key;
}
