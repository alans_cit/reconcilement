package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type V tex order.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VTexOrder {
    private List<String> paymentIds;
    private List<Chargeback> chargebacks;
    private String code;
    private String status;
    private String description;
    private String statusTime;
    private Payload payload;
}
