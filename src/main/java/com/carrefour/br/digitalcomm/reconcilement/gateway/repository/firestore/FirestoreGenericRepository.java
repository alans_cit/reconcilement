package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * The type Firestore generic repository.
 *
 * @param <T> the type parameter
 */
public abstract class FirestoreGenericRepository<T> {
    /**
     * The Collection.
     */
    protected final String collection;
    /**
     * The Firestore.
     */
    protected final Firestore firestore;

    /**
     * Instantiates a new Firestore generic repository.
     *
     * @param collection the collection
     * @param firestore  the firestore
     */
    public FirestoreGenericRepository(String collection, Firestore firestore) {
        this.collection = collection;
        this.firestore = firestore;
    }

    /**
     * Gets first.
     *
     * @return the first
     */
    public T getFirst() {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return convertToObject(querySnapshot.get().getDocuments().get(0));
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Gets first by.
     *
     * @param parameterName the parameter name
     * @param value         the value
     * @return the first by
     */
    public T getFirstBy(String parameterName, Object value) {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .whereEqualTo(parameterName, value)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return convertToObject(querySnapshot.get().getDocuments().get(0));
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Gets by.
     *
     * @param parameterName the parameter name
     * @param value         the value
     * @return the by
     */
    public List<T> getBy(String parameterName, String value) {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .whereEqualTo(parameterName, value)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return querySnapshot.get().getDocuments().stream()
                        .map(this::convertToObject)
                        .collect(Collectors.toList());
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Gets by.
     *
     * @param parameterName the parameter name
     * @param value         the value
     * @return the by
     */
    public List<T> getBy(String parameterName, boolean value) {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .whereEqualTo(parameterName, value)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return querySnapshot.get().getDocuments().stream()
                        .map(this::convertToObject)
                        .collect(Collectors.toList());
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Gets by array contains.
     *
     * @param parameterName the parameter name
     * @param value         the value
     * @return the by array contains
     */
    public List<T> getByArrayContains(String parameterName, String value) {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .whereArrayContains(parameterName, value)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return querySnapshot.get().getDocuments().stream()
                        .map(this::convertToObject)
                        .collect(Collectors.toList());
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Gets all.
     *
     * @return the all
     */
    public List<T> getAll() {
        try {
            final ApiFuture<QuerySnapshot> querySnapshot = firestore.collection(this.collection)
                    .get();
            if (!querySnapshot.get().isEmpty()) {
                return querySnapshot.get().getDocuments().stream()
                        .map(this::convertToObject)
                        .collect(Collectors.toList());
            }
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
        return null;
    }

    /**
     * Save.
     *
     * @param entity the entity
     */
    public void save(T entity) {
        firestore.collection(this.collection).add(entity);
    }

    /**
     * Update.
     *
     * @param entity    the entity
     * @param childPath the child path
     */
    public void update(T entity, String childPath) {
        firestore.collection(this.collection).document(childPath).set(entity, SetOptions.merge());
    }

    /**
     * Convert to object t.
     *
     * @param documentSnapshot the document snapshot
     * @return the t
     */
    protected T convertToObject(QueryDocumentSnapshot documentSnapshot) {
        return documentSnapshot.toObject((Class<T>)
            ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0]);
    }
}
