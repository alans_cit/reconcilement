package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type User feign response.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserFeignResponse {
    private String id;
    private String name;
    private String email;
    private String phone;
}
