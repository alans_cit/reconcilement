package com.carrefour.br.digitalcomm.reconcilement.presentation.entities.balance;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * The interface V tex order balance request mapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VTexOrderBalanceRequestMapper {

    /**
     * The constant INSTANCE.
     */
    VTexOrderBalanceRequestMapper INSTANCE = Mappers.getMapper(VTexOrderBalanceRequestMapper.class);

    /**
     * Request to entity v tex order balance.
     *
     * @param request the request
     * @return the v tex order balance
     */
    VTexOrderBalance requestToEntity(VTexOrderBalanceRequest request);
}
