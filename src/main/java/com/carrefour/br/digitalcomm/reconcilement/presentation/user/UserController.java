package com.carrefour.br.digitalcomm.reconcilement.presentation.user;

import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.user.UserResponse;
import com.carrefour.br.digitalcomm.reconcilement.presentation.entities.user.UserResponseMapper;
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.UserNotFoundException;
import com.carrefour.br.digitalcomm.reconcilement.usecase.user.GetAllUsersUC;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type User controller.
 */
@Slf4j
@RestController
@RequestMapping(value = "/v1/reconcilement")
@RequiredArgsConstructor
public class UserController {
    private final GetAllUsersUC getAllUsersUC;

    /**
     * Gets all users.
     *
     * @return the all users
     */
    @GetMapping("/users")
    @ResponseStatus(HttpStatus.OK)
    public List<UserResponse> getAllUsers() {
        log.info("getting list of user");
        return UserResponseMapper.INSTANCE.entityToResponse(getAllUsersUC.execute());
    }

    /**
     * Handle user not found exception response entity.
     *
     * @param e the e
     * @return the response entity
     */
    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<Void> handleUserNotFoundException(UserNotFoundException e) {
        log.error("user not found");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
