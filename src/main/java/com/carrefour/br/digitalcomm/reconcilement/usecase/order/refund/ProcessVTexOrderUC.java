package com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund;

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.Payment;
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder;
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAcceptedUC;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Process v tex order uc.
 */
@Slf4j
@RequiredArgsConstructor
public class ProcessVTexOrderUC {
    private final SaveVTexOrder saveVTexOrder;
    private final GetListOfStatusOrderAcceptedUC getListOfStatusOrderAcceptedUC;

    /**
     * Execute.
     *
     * @param vTexOrder the v tex order
     */
    public void execute(VTexOrder vTexOrder) {
        log.info("processing VTexOrder");

        if(!Objects.isNull(vTexOrder)
                && Arrays.asList(getListOfStatusOrderAcceptedUC.execute()).contains(vTexOrder.getStatus())) {
            List<String> paymentIds = vTexOrder.getPayload()
                    .getPayments()
                    .stream()
                    .map(Payment::getPaymentId)
                    .collect(Collectors.toList());

            vTexOrder.setPaymentIds(paymentIds);

            saveVTexOrder.save(vTexOrder);
        }
    }
}
