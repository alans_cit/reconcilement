package com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Payment.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
    private String paymentId;
    private String paymentSystem;
    private String cardNumber;
    private Long value ;
}
