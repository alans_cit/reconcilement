package com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Shipping address document.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShippingAddressDocument {
    private String addressType;
    private String postalCode;
    private String state;
    private String neighborhood;
    private String complement;
    private String number;
    private String country;
    private String reference;
    private String street;
    private String city;
}
