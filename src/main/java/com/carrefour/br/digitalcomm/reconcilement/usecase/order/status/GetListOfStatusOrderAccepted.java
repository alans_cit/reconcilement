package com.carrefour.br.digitalcomm.reconcilement.usecase.order.status;

import java.util.List;

/**
 * The interface Get list of status order accepted.
 */
public interface GetListOfStatusOrderAccepted {
    /**
     * Gets status accepted.
     *
     * @return the status accepted
     */
    List<String> getStatusAccepted();
}
