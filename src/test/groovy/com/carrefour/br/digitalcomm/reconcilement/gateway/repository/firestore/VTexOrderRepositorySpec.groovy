package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocument
import com.google.cloud.firestore.Firestore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
class VTexOrderRepositorySpec extends Specification {

    @Autowired
    private VTexOrderRepository repository

    @Autowired
    private Firestore firestore

    @Shared
    def ids = []

    def cleanup() {
        ids.each {
            firestore.collection(VTexOrderDocument.COLLECTION_NAME).document(it).delete()
        }
    }

    def waitFor() {
        Thread.sleep(5000)
    }

    def "Try to find a existent Order by status will successfully"() {
        given: "A valid document"
        String code = "8c2fcada-aeee-4f50-bc48-8912d5745504"
        String status = "blinking"
        VTexOrderDocument document = VTexOrderDocument.builder()
                .code(code)
                .status(status)
                .build()
        firestore.collection(VTexOrderDocument.COLLECTION_NAME).add(document)

        waitFor()

        when: "I get by status"
        List<VTexOrderDocument> result = repository.getBy(VTexOrderDocument.STATUS, status)

        then: "Must be return an expected object"
        result != null
        result.get(0).code == code
        result.get(0).status == status
        ids << result.get(0).id
    }

    def "Try to find a non existent Order by status will return null"() {
        given: "A valid code document"
        String status = "void"

        when: "I get by status"
        List<VTexOrderDocument> result = repository.getBy(VTexOrderDocument.STATUS, status)

        then: "Must be return an null object"
        result == null
    }
}
