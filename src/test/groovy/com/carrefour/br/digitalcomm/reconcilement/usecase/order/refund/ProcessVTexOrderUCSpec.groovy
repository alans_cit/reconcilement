package com.carrefour.br.digitalcomm.reconcilement.usecase.order.refund

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.Payload
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.Payment
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder
import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAcceptedUC
import spock.lang.Specification

class ProcessVTexOrderUCSpec extends Specification {
    private SaveVTexOrder saveVTexOrder = Mock()
    private GetListOfStatusOrderAcceptedUC getListOfStatusOrderAcceptedUC = Mock()
    private ProcessVTexOrderUC uc = new ProcessVTexOrderUC(saveVTexOrder, getListOfStatusOrderAcceptedUC)

    def setup() {}

    def "Call the use case without a list of status accepted"() {
        given: "The mock"
        String paymentId = "123"
        String status = "PCP"

        1 * getListOfStatusOrderAcceptedUC.execute() >> {
            return null
        }

        0 * saveVTexOrder.save(_ as VTexOrder) {}
        VTexOrder vTexOrder = VTexOrder.builder()
                .status(status)
                .payload(Payload.builder()
                        .payments(Arrays.asList(Payment.builder()
                                .paymentId(paymentId)
                                .build()))
                        .build())
                .build()

        when: "I call the use case"
        uc.execute(vTexOrder)

        then: "I want a result"
        notThrown(Exception)
    }

    def "Call the use case with a list of status accepted"() {
        given: "The mock"
        String paymentId = "123"
        String status = "PCP"

        1 * getListOfStatusOrderAcceptedUC.execute() >> {
            return Arrays.asList(status)
        }

        0 * saveVTexOrder.save(_ as VTexOrder) {}
        VTexOrder vTexOrder = VTexOrder.builder()
                .status(status)
                .payload(Payload.builder()
                        .payments(Arrays.asList(Payment.builder()
                                .paymentId(paymentId)
                                .build()))
                        .build())
                .build()

        when: "I call the use case"
        uc.execute(vTexOrder)

        then: "I want a result"
        notThrown(Exception)
    }
}
