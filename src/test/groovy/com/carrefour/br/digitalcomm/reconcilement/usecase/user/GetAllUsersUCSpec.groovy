package com.carrefour.br.digitalcomm.reconcilement.usecase.user

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.UserNotFoundException
import spock.lang.Specification

class GetAllUsersUCSpec extends Specification {
    private GetAllUsers getAllUsers = Mock()
    private  GetAllUsersUC uc = new  GetAllUsersUC(getAllUsers)

    def setup() {}

    def "Calling the use case without results will return a null result"() {
        given: "The mock"
        1 * getAllUsers.getAll() >> {
            return null
        }

        when: "I call the use case"
        List<User> result = uc.execute()

        then: "I want a result"
        thrown(UserNotFoundException)
    }

    def "Calling the use case with results will return a result"() {
        given: "The mock"
        1 * getAllUsers.getAll() >> {
            return Arrays.asList(User.builder().id(UUID.randomUUID().toString()).build())

        }

        when: "I call the use case"
        List<User> result = uc.execute()

        then: "I want a result"
        result != null
        result.size() == 1
    }
}
