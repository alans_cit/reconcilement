package com.carrefour.br.digitalcomm.reconcilement.gateway.order

import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.config.OrderStatusRepository
import spock.lang.Specification

import static org.hamcrest.Matchers.containsInAnyOrder
import static spock.util.matcher.HamcrestSupport.that

class OrderStatusGatewaySpec extends Specification {
    private OrderStatusRepository repository = Mock()
    private OrderStatusGateway gw = new OrderStatusGateway(repository)

    def "Call the gateway without a list of status accepted"() {
        given: "The Mock"
        1 * repository.getStatusAccepted() >> {
            return null
        }

        when: "I call the gateway"
        List<String> result = gw.getStatusAccepted()

        then: "I want a result"
        result == null
    }

    def "Call the gateway with a list of status accepted"() {
        given: "The Mock"
        List<String> statusList = Arrays.asList("TOC", "TIC", "TUC")

        1 * repository.getStatusAccepted() >> {
            return statusList
        }

        when: "I call the gateway"
        List<String> result = gw.getStatusAccepted()

        then: "I want a result"
        result != null
        that (result, containsInAnyOrder("TOC", "TIC", "TUC"))
    }
}
