package com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.InvalidVTexOrderBalanceException
import spock.lang.Specification

class GenerateBalanceUCSpec extends Specification {
    private PublishVTexOrderBalanceInTopic publishVTexOrderBalanceInTopic = Mock()
    private ValidateVTexOrderBalanceBeforePublishUC validateVTexOrderBalanceBeforePublishUC = new ValidateVTexOrderBalanceBeforePublishUC()
    private GenerateBalanceUC uc = new GenerateBalanceUC(publishVTexOrderBalanceInTopic, validateVTexOrderBalanceBeforePublishUC)

    def setup() {}

    def "Call the use case with a valid order balance"() {
        given: "The mock"
        VTexOrderBalance orderBalance = VTexOrderBalance.builder()
                .code("123")
                .status("PRO")
                .build()

        1 * publishVTexOrderBalanceInTopic.publish(orderBalance) >> {
            return void
        }

        when: "I call the use case"
        uc.execute(orderBalance)

        then: "I want the result"
        notThrown(InvalidVTexOrderBalanceException)
    }
}
