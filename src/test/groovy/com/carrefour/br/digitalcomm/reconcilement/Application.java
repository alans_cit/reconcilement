package com.carrefour.br.digitalcomm.reconcilement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * The type Application.
 */
@EnableConfigurationProperties()
@SpringBootApplication(scanBasePackages = "com.carrefour.*")
public class Application {
    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
