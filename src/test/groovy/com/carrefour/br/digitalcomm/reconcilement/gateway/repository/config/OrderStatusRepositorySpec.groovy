package com.carrefour.br.digitalcomm.reconcilement.gateway.repository.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class OrderStatusRepositorySpec extends Specification {

    @Autowired
    private OrderStatusRepository repository

    def "Try to find a existent status will successfully"() {
        given: "Nothing"

        when: "I Call the repository"
        List<String> result = repository.getStatusAccepted()

        then: "I want a result"
        result != null
        !result.isEmpty()
    }
}
