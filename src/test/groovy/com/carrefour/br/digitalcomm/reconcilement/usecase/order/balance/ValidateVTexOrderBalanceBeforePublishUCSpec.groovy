package com.carrefour.br.digitalcomm.reconcilement.usecase.order.balance

import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.balance.VTexOrderBalance
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.InvalidVTexOrderBalanceException
import spock.lang.Specification

class ValidateVTexOrderBalanceBeforePublishUCSpec extends Specification {
    private ValidateVTexOrderBalanceBeforePublishUC uc = new ValidateVTexOrderBalanceBeforePublishUC()

    def setup() {}

    def "Validate a invalid entity"() {
        given: "A entity"
        VTexOrderBalance entity = VTexOrderBalance.builder().build()

        when: "Call the use case"
        boolean result = uc.execute(entity)

        then: "I want the result"
        thrown(InvalidVTexOrderBalanceException)
    }

    def "Validate a valid entity"() {
        given: "A entity"
        VTexOrderBalance entity = VTexOrderBalance.builder()
                .code("1")
                .status("POW")
                .build()

        when: "Call the use case"
        boolean result = uc.execute(entity)

        then: "I want the result"
        notThrown(InvalidVTexOrderBalanceException)
        result
    }
}
