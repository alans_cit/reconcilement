package com.carrefour.br.digitalcomm.reconcilement.presentation.user


import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User
import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.UserNotFoundException
import com.carrefour.br.digitalcomm.reconcilement.usecase.user.GetAllUsersUC
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@WebMvcTest(value = UserController)
@EnableAspectJAutoProxy
class UserControllerSpec extends Specification {
    @MockBean
    private GetAllUsersUC getAllUsersUC
    private UserController controller
    private MockMvc mvc

    def setup() {
        this.getAllUsersUC = Mock(GetAllUsersUC)
        this.controller = new UserController(getAllUsersUC)
        this.mvc = MockMvcBuilders.standaloneSetup(controller).build()
    }

    def "Call the controller without a list of users"() {
        given: "The mock"
        1 * getAllUsersUC.execute() >> {
            throw new UserNotFoundException()
        }

        when: "I call the controller"
        MvcResult result = mvc.perform(get("/v1/reconcilement/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()

        then: "I want a result"
        with(result.getResponse()) {
            status == HttpStatus.NOT_FOUND.value()
        }
    }

    def "Call the controller with a list of users"() {
        given: "The mock"
        List<User> userList = Arrays.asList(User.builder()
                .id(UUID.randomUUID().toString())
                .name("Paul")
                .email("email@email.gov.br")
                .build())
        1 * getAllUsersUC.execute() >> {
            return userList
        }

        when: "I call the controller"
        MvcResult result = mvc.perform(get("/v1/reconcilement/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()

        then: "I want a result"
        with(result.getResponse()) {
            status == HttpStatus.OK.value()

            contentAsString.contains(userList.get(0).id)
        }
    }
}
