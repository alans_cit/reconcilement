package com.carrefour.br.digitalcomm.reconcilement.presentation.order


import com.carrefour.br.digitalcomm.reconcilement.usecase.order.status.GetListOfStatusOrderAcceptedUC
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification
import  com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.StatusOrderAcceptedNotFoundException

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@WebMvcTest(value = AcceptedStatusController)
@EnableAspectJAutoProxy
class AcceptedStatusControllerSpec extends Specification {

    @MockBean
    private GetListOfStatusOrderAcceptedUC getListOfStatusOrderAcceptedUC
    private AcceptedStatusController controller
    private MockMvc mvc

    def setup() {
        this.getListOfStatusOrderAcceptedUC = Mock(GetListOfStatusOrderAcceptedUC)
        this.controller = new AcceptedStatusController(getListOfStatusOrderAcceptedUC)
        this.mvc = MockMvcBuilders.standaloneSetup(controller).build()
    }

    def "Call the controller without a list of status accepted"() {
        given: "The mock"
        1 * getListOfStatusOrderAcceptedUC.execute() >> {
            throw new StatusOrderAcceptedNotFoundException()
        }

        when: "I call the controller"
        MvcResult result = mvc.perform(get("/v1/reconcilement/accepted-status")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()

        then: "I want a result"
        with(result.getResponse()) {
            status == HttpStatus.NOT_FOUND.value()
        }
    }

    def "Call the controller with a list of status accepted"() {
        given: "The mock"
        List<String> stringList = Arrays.asList("CG", "ML")
        1 * getListOfStatusOrderAcceptedUC.execute() >> {
            return stringList
        }

        when: "I call the controller"
        MvcResult result = mvc.perform(get("/v1/reconcilement/accepted-status")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()

        then: "I want a result"
        with(result.getResponse()) {
            status == HttpStatus.OK.value()

            contentAsString.contains(stringList.get(0))
            contentAsString.contains(stringList.get(1))
        }
    }
}
