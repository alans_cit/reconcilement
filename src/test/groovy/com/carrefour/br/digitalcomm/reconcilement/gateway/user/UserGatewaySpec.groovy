package com.carrefour.br.digitalcomm.reconcilement.gateway.user

import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.user.UserFeignResponse
import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.feign.PlaceHolderUserRepository
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.user.User
import spock.lang.Specification

class UserGatewaySpec extends Specification {
    private PlaceHolderUserRepository repository = Mock()
    private UserGateway gw = new UserGateway(repository)

    def "Call the gateway without a list of user"() {
        given: "The Mock"
        1 * repository.getAllUsers() >> {
            return null
        }

        when: "I call the gateway"
        List<String> result = gw.getAll()

        then: "I want a result"
        result == null
    }

    def "Call the gateway with a list of user"() {
        given: "The Mock"
        List<UserFeignResponse> userList = Arrays.asList(UserFeignResponse.builder().id(UUID.randomUUID().toString()).build(), UserFeignResponse.builder().id(UUID.randomUUID().toString()).build())

        1 * repository.getAllUsers() >> {
            return userList
        }

        when: "I call the gateway"
        List<User> result = gw.getAll()

        then: "I want a result"
        result != null
        result.size() == 2
    }
}
