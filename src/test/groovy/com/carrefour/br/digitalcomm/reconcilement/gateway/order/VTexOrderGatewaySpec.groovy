package com.carrefour.br.digitalcomm.reconcilement.gateway.order

import com.carrefour.br.digitalcomm.reconcilement.gateway.repository.firestore.VTexOrderRepository
import com.carrefour.br.digitalcomm.reconcilement.gateway.entities.order.VTexOrderDocument
import com.carrefour.br.digitalcomm.reconcilement.usecase.entities.order.VTexOrder
import spock.lang.Specification

class VTexOrderGatewaySpec extends Specification {
    private VTexOrderRepository repository = Mock()
    private VTexOrderGateway gw = new VTexOrderGateway(repository)

    def "Try to find a existent order by status id will successfully"() {
        given: "A valid document"
        String code = "fcd81096-65d2-4ed8-bd5f-4ac2a75a07b2"
        String status = "dancing"

        and: "should call gateway"
        repository.getBy(VTexOrderDocument.STATUS, status) >> {
            return Arrays.asList(VTexOrderDocument.builder()
                    .code(code)
                    .status(status)
                    .build())
        }

        when: "I get by payment id"
        List<VTexOrder> result = gw.getByStatus(status)

        then: "Must be return an expected object"
        result != null
        result.get(0).code == code
        result.get(0).status == status
    }

    def "Try to find a non existent order by status id will return null"() {
        given: "A valid paymentId document"
        String status = "Xamberlau"

        and: "should call gateway"
        repository.getBy(VTexOrderDocument.STATUS, status) >> {
            return null
        }

        when: "I get by status"
        List<VTexOrder> result = gw.getByStatus(status)

        then: "Must be return an null object"
        result == null
    }

    def "Try to save a order will successfully"() {
        given: "A valid order"
        String code = "e2217331-bd35-49be-9dd5-e9657c1bdc4f"
        String status = "steeving"

        VTexOrder order = VTexOrder.builder()
                .code(code)
                .status(status)
                .build()

        when: "I save using the gateway"
        gw.save(order)

        then: "Must be save object"
        1 * repository.save(_) >> {
        }
    }
}
