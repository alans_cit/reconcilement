package com.carrefour.br.digitalcomm.reconcilement.usecase.order.status

import com.carrefour.br.digitalcomm.reconcilement.usecase.exceptions.StatusOrderAcceptedNotFoundException
import spock.lang.Specification

class GetListOfStatusOrderAcceptedUCSpec extends Specification {
    private GetListOfStatusOrderAccepted getListOfStatusOrderAccepted = Mock()
    private GetListOfStatusOrderAcceptedUC uc = new GetListOfStatusOrderAcceptedUC(getListOfStatusOrderAccepted)

    def setup() {}

    def "Calling the use case without results will return a null result"() {
        given: "The mock"
        1 * getListOfStatusOrderAccepted.getStatusAccepted() >> {
            return null
        }

        when: "I call the use case"
        List<String> result = uc.execute()

        then: "I want a result"
        thrown(StatusOrderAcceptedNotFoundException)
    }

    def "Calling the use case with results will return a result"() {
        given: "The mock"
        1 * getListOfStatusOrderAccepted.getStatusAccepted() >> {
            return Arrays.asList("Vulcan")
        }

        when: "I call the use case"
        List<String> result = uc.execute()

        then: "I want a result"
        result != null
        result.size() == 1
        result.get(0) == "Vulcan"
    }
}
